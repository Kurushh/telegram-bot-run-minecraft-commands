import datetime
import subprocess
from telegram.ext import CommandHandler, Updater, CallbackQueryHandler

#Variables
mytoken = ""
chat_id=""
pauset=datetime.datetime.now()
HOST=$MCRCON_HOST
PORT=$MCRCON_PORT
PASSWD=$MCRCON_PASS

PSCRIPT="path/to/start.sh"
# start.sh is the script that starts the spigot server


# All commands runned through subprocess.run() or subprocess.Popen() are linux/unix commands
def deadOrAlive():
    out = subprocess.run(["mcrcon","-H",HOST, "-P" ,PORT ,"-p" ,PASSWD ,"list"],capture_output=True)
    if ( len(out.stderr) == 0):
        return True
    else:
        return False

def isAlive(update, context):
    if (deadOrAlive() == True ):
        update.message.reply_text("Server is alive")
    else:
        update.message.reply_text("Server is dead")

def check_server(context):
        if (deadOrAlive() == False ):
            return
        subprocess.run(["mcrcon","-H",HOST, "-P" ,PORT,"-p", PASSWD ,"save-all"],capture_output=True)
        out = subprocess.run(["mcrcon","-H",HOST, "-P" ,PORT ,"-p" ,PASSWD ,"list"],capture_output=True)

        #Output ends with char '\n' and it's a stream of byte, decode it to utf-8
        format_out = out.stdout[0:len(out.stdout)-1].decode()
        n_players_on = int(format_out[10:12])

        if (n_players_on == 0):
            out = subprocess.run(["mcrcon","-H",HOST, "-P" , PORT ,"-p" ,PASSWD ,"stop"],capture_output=True)
            if ( len(out.stderr) != 0):
                print("Critical error")

def kong(update, context):
    id = str(update.message.chat_id)
    if ( id != chat_id):
        update.message.reply_text("Unauthorized")
        return

    update.message.reply_text("kong"); #ping pong but reskined

def start(update, context):
    global pauset
    id = str(update.message.chat_id)
    if ( id != chat_id):
        update.message.reply_text("Unauthorized")
        return

    now = datetime.datetime.now()
    if (now < pauset):
        update.message.reply_text("Server is going to start...")
        return


    if (deadOrAlive() == True ):
        update.message.reply_text("Server already online")
        return

    #.Popen because it creates a child process
    out = subprocess.Popen(PSCRIPT)
    print(out)
    context.bot.send_message(chat_id=chat_id, text = "Starting the server!")
    pauset = datetime.datetime.now() + datetime.timedelta(minutes=12) #command cooldown
